## Does Character need to be abstract?

A. No, we can just modify Character(Cell location) to take a Colour input: Character(Cell location, Color color)
But by having it abstract, we can keep track of our character objects more easily, and add extra methods to
individual characters without affecting the others.