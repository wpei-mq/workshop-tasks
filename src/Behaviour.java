import bos.RelativeMove;

public interface Behaviour {
    RelativeMove chooseMove(Stage stage, Character mover);
}
