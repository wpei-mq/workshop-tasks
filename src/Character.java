import bos.GamePiece;

import java.awt.*;
import java.util.Optional;

public abstract class Character implements GamePiece<Cell>{
    Optional<Color> display;    // Field for an Optional object of type Color
    Cell location;              // Field for the Cell object
    Behaviour behaviour;

    public Character(Cell location){
        this.location = location;
        this.display = Optional.empty(); // assign an empty Optional object to display
    }

    public Character(Cell location, Behaviour behaviour) {
        this.location = location;
        this.behaviour = behaviour;
        this.display = Optional.empty();
    }

    // because all subclasses need getLocationOf and setLocationOf functions,
    // they can be defined in the abstract class
    @Override
    public Cell getLocationOf() {
        return location;
    }

    @Override
    public void setLocationOf(Cell c) {
        location = c;
    }

    public void paint(Graphics g){
        g.setColor(display.orElse(Color.MAGENTA));    // set default color to Magenta
        g.fillOval(location.x + location.width/4, location.y+ location.height/4,
                location.width/2, location.height/2);
    }
}
