import bos.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Grid implements GameBoard<Cell>{

    private Cell[][] cells  = new Cell[20][20];

    private int x;
    private int y;

    public Grid(int x, int y) {
        this.x = x;
        this.y = y;

        for(int i = 0; i < 20; i++) {
            for(int j = 0; j < 20; j++) {
                cells[i][j] = new Cell(x + j * 35, y + i * 35);
            }
        }
    }

    public void paint(Graphics g, Point mousePosition) {
        doToEachCell(cell -> cell.paint(g, cell.contains(mousePosition)));
    }

    public Cell getRandomCell(){
        java.util.Random rand = new java.util.Random();
        return cells[rand.nextInt(20)][rand.nextInt(20)];
    }

    /**
     * Takes a cell consumer (i.e. a function that has a single 'Cell' argument and
     * returns 'void' ) and applies that consumer to each cell in the grid.
     * @param func The 'Cell' to 'void' function to apply at each spot
     */
    public void doToEachCell(Consumer<Cell> func) {
        for(int y = 0; y < 20; ++y) {
            for(int x = 0; x < 20; ++x) {
                func.accept(cells[x][y]);
            }
        }
    }

    /** Takes a cell predicate (i.e. a function that has a single `Cell` argument and
     * returns a `boolean` result) and applies that predicate to each cell, returning
     * the first cell it finds for which the predicate is true.
     * @param predicate The `Cell` to `boolean` function to test with
     * @return The first cell (searching row by row, left to right) that is true for the predicate.  Returns `null` if no such cell found.
     */
    public Pair<Integer, Integer> findAmongstCells(Predicate<Cell> predicate){
        // Your job to fill in the body
        for(int y = 0; y < 20; ++y) {
            for(int x = 0; x < 20; ++x) {
                if (predicate.test(cells[x][y])) {
                    return new Pair(x, y);
                }
            }
        }
        return null;
    }

    /** Takes a cell predicate (i.e. a function that has a single `Cell` argument and
     * returns a `result` and applies that predicate to each cell, returning
     * the first cell it finds for which the predicate is true.  Returns an optional that is full
     * if such a cell is found and an empty optional if there is no such cell.
     * @param predicate The `Cell` to `boolean` function to test with
     * @return The first cell (searching row by row, left to right) that is true for the predicate.  Returns an empty optional if no cell found.
     */
    public Optional<Pair<Integer, Integer>> safeFindAmongstCells(Predicate<Cell> predicate){
        // Your job to fill in the body
        for(int y = 0; y < 20; ++y) {
            for(int x = 0; x < 20; ++x) {
                if (predicate.test(cells[x][y])) {
                    return Optional.of(new Pair(x, y));
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public Optional<Cell> below(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo).filter((p) -> p.first < 19)
                                                            .map   ((p) -> cells[p.first+1][p.second]);
    }

    //implement GameBoard interface methods
    @Override
    public Optional above(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo).filter((p) -> p.first > 0)
                .map   ((p) -> cells[p.first-1][p.second]);
    }

    @Override
    public Optional rightOf(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo).filter((p) -> p.second < 19)
                .map   ((p) -> cells[p.first][p.second+1]);
    }

    @Override
    public Optional leftOf(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo).filter((p) -> p.second > 0)
                .map   ((p) -> cells[p.first][p.second-1]);
    }

    @Override
    public List<RelativeMove> movesBetween(Cell from, Cell to, GamePiece<Cell> gamePiece) {
        Pair<Integer, Integer> fromIndex = findAmongstCells((c) -> c == from);
        Pair<Integer, Integer> toIndex = findAmongstCells((c) -> c == to);

        // always use the highest super class on the left and the implementation needed on the right
        List<RelativeMove> result = new ArrayList<>();

        // horizontal movement
        if (fromIndex.second <= toIndex.second) {
            //move right
            for (int i = fromIndex.second; i < toIndex.second; i++) {
                result.add(new MoveRight(this, gamePiece));
            }
        } else {
            //move left
            for (int i = fromIndex.second; i > toIndex.second; i--) {
                result.add(new MoveLeft(this, gamePiece));
            }
        }

        // vertical movement
        if (fromIndex.first <= toIndex.first) {
            //move right
            for (int i = fromIndex.first; i < toIndex.first; i++) {
                result.add(new MoveDown(this, gamePiece));
            }
        } else {
            //move left
            for (int i = fromIndex.first; i > toIndex.first; i--) {
                result.add(new MoveUp(this, gamePiece));
            }
        }
        return result;
    }
}
