import bos.GameBoard;

import java.awt.event.KeyEvent;

public interface Observer {
    void update(GameBoard<Cell> grid, KeyEvent e);
}
