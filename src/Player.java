import bos.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

public class Player implements Observer, GamePiece<Cell> {

    protected Cell location;
    private Optional<Color> display;
    private KeyEvent key;
    List<RelativeMove> moves = new ArrayList<>();

    public Player(Cell location) {
        this.location = location;
        this.display = Optional.empty();
    }

    public boolean inMove() {
        if (key == null) {
            System.out.println("Waiting");
            return true;
        }
        return false;
    }

    public RelativeMove startMove(GameBoard grid, KeyEvent e) {
        if (e != null) {
            System.out.println(e.getKeyChar());
            switch (e.getKeyChar()) {
                case 'w':
                    moves.add(new MoveUp(grid, this));
                    key = e;
                    break;
                case 's':
                    moves.add(new MoveDown(grid, this));
                    key = e;
                    break;
                case 'a':
                    moves.add(new MoveLeft(grid, this));
                    key = e;
                    break;
                case 'd':
                    moves.add(new MoveRight(grid, this));
                    key = e;
                    break;
            }
        } else {
            moves.add(new NoMove(grid, this));
            key = null;
        }
        return moves.get(0);
    }

    public void paint(Graphics g){
        g.setColor(display.orElse(Color.ORANGE));    // set default color to Magenta
        g.fillOval(location.x + location.width/4, location.y+ location.height/4,
                location.width/2, location.height/2);
    }

    @Override
    public void update(GameBoard grid, KeyEvent e) {
        this.startMove(grid, e);
    }

    @Override
    public Cell getLocationOf() {
        return this.location;
    }

    @Override
    public void setLocationOf(Cell location) {
        this.location = location;
    }
}
