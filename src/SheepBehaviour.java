import bos.NoMove;
import bos.RelativeMove;

import java.util.List;

public class SheepBehaviour implements Behaviour {
    @Override
    public RelativeMove chooseMove(Stage stage, Character mover) {
        List<RelativeMove> possibles = stage.grid.movesBetween(mover.location, stage.shepherd.location, mover);
        if (possibles.size() == 0) {
            return new NoMove(stage.grid, mover);
        }
        if (mover.location.x + 1 == mover.location.y + 1) {
            return new NoMove(stage.grid, mover);
        }
        return possibles.get(0);
    }
}
