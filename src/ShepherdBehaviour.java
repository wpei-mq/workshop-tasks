import bos.NoMove;
import bos.RelativeMove;

import java.util.List;

public class ShepherdBehaviour implements Behaviour {
    @Override
    public RelativeMove chooseMove(Stage stage, Character mover) {
        if (stage.sheep.location.x == stage.sheep.location.y) {
            List<RelativeMove> possibles = stage.grid.movesBetween(mover.location, stage.sheep.location, mover);
            if (possibles.size() == 0) {
                return new NoMove(stage.grid, mover);
            } else {
                return possibles.get(0);
            }
        }
        return new NoMove(stage.grid, mover);
    }
}
