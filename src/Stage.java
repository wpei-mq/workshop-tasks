import bos.RelativeMove;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Stage {
    protected Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    protected Player player;
    protected List<RelativeMove> playerMoves;
    private List<Observer> observers = new ArrayList<>();
    private KeyEvent key;

    private Instant timeOfLastMove;

    public Stage() {
        grid     = new Grid(10, 10);
        sheep    = new Sheep(grid.getRandomCell(), new SheepBehaviour());
        shepherd = new Shepherd(grid.getRandomCell(), new ShepherdBehaviour());
        wolf     = new Wolf(grid.getRandomCell(), new WolfBehaviour());
        player   = new Player(grid.getRandomCell());

        observers.add(player);
    }

    public void setKey(KeyEvent key) {
        this.key = key;
    }

    public KeyEvent getKey() {
        return key;
    }

    public void notifyAllObservers(){
        for (Observer observer : observers) {
            observer.update(grid, key);
        }
    }

    public void update() {
        if (sheep.location == shepherd.location) {
            System.out.println("Sheep is safe");
            System.exit(0);
        } else if (sheep.location == wolf.location){
            System.out.println("Sheep is dead");
            System.exit(1);
        } else if (timeOfLastMove.plus(Duration.ofSeconds(2)).isBefore(Instant.now())) {
            if (player.inMove()) {
                notifyAllObservers();
            }
            timeOfLastMove = Instant.now();
            sheep.behaviour.chooseMove(this, sheep).perform();
            shepherd.behaviour.chooseMove(this, shepherd).perform();
            wolf.behaviour.chooseMove(this, wolf).perform();
        }
    }

    public void paint(Graphics g, Point mouseLocation) {
        if (timeOfLastMove == null) {
            timeOfLastMove = Instant.now();
        }
        grid.paint(g, mouseLocation);
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        player.paint(g);
        update();
    }
}