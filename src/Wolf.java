import java.awt.*;
import java.util.Optional;

public class Wolf extends Character {

    public Wolf(Cell location) {
        super(location);
        display = Optional.of(Color.RED);
    }

    public Wolf(Cell location, Behaviour behaviour) {
        super(location, behaviour);
        display = Optional.of(Color.RED);
    }
}