import bos.NoMove;
import bos.RelativeMove;

import java.util.List;

public class WolfBehaviour implements Behaviour {
    @Override
    public RelativeMove chooseMove(Stage stage, Character mover) {
        List<RelativeMove> possibles = stage.grid.movesBetween(mover.location, stage.sheep.location, mover);
        if (possibles.size() == 0) {
            return new NoMove(stage.grid, mover);
        }
        return possibles.get(0);
    }
}
